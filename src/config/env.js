/**
 * baseUrl: 域名地址
 * routerMode: 路由模式
 * imgBaseUrl: 图片所在域名地址
 */

let baseUrl;
const imgBaseUrl = 'https://fuss10.elemecdn.com';
if (process.env.NODE_ENV == 'development') {
    baseUrl = '';
}

export {
    baseUrl,
    imgBaseUrl
}